#include <SoftwareSerial.h>
#include <TinyGPSPlus.h>
#include <HardwareSerial.h>
#include <GSMSimHTTP.h>
#include <ArduinoJson.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#define bleServerName "gps_device"
#define SERVICE_UUID "c93bcec0-b099-479d-b455-0a00dfa81b30"
BLECharacteristic textUUID("28612975-db8e-4870-aa13-18a1c293a062");
BLECharacteristic messageIdUUID("3c0faebf-9417-4ef5-a97e-bb6634780d3d");
BLEDescriptor textDescriptor(BLEUUID((uint16_t)0x2902));
BLEDescriptor messageIdDescriptor(BLEUUID((uint16_t)0x2902));

bool deviceConnected = false;

class MyServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    deviceConnected = true;
  };
  void onDisconnect(BLEServer* pServer) {
    deviceConnected = false;
  }
};

static const int RXPin = 32, TXPin = 33;

#define RESET_PIN 23
#define GSM_RX_PIN 26
#define GSM_TX_PIN 27

static const uint32_t GPSBaud = 9600;
GSMSimHTTP http(Serial1, RESET_PIN);
HardwareSerial ss(2);
TinyGPSPlus gps;
int device_id = 1;

void setup() {

  BLEDevice::init(bleServerName);
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  BLEService *bmeService = pServer->createService(SERVICE_UUID);

  bmeService->addCharacteristic(&textUUID);
  textDescriptor.setValue("Test text");
  textUUID.addDescriptor(&textDescriptor);

  bmeService->addCharacteristic(&messageIdUUID);
  messageIdDescriptor.setValue("Test text");
  messageIdUUID.addDescriptor(&messageIdDescriptor);

  bmeService->start();

  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");

  ss.begin(9600, SERIAL_8N1, RXPin, TXPin);
  Serial1.begin(115200,SERIAL_8N1,26,27);
  while(!Serial1) {
    ; // wait for module for connect.
  }
  //SIM900.begin(9600);
  Serial.begin(115200);
  Serial.println("STARTED");
  
  http.init();
  delay(20000);

  Serial.print("Set Phone Function... ");
  Serial.println(http.setPhoneFunc(1));


  Serial.print("is Module Registered to Network?... ");
  Serial.println(http.isRegistered());

  Serial.print("Signal Quality... ");
  Serial.println(http.signalQuality());

  Serial.print("Operator Name... ");
  Serial.println(http.operatorNameFromSim());

  Serial.print("GPRS Init... ");
  http.gprsInit("internet.yota"); // Its optional. You can set apn, user and password with this method. Default APN: "internet" Default USER: "" Default PWD: ""

  Serial.print("Connect GPRS... ");
  Serial.println(http.connect());

  Serial.print("Get IP Address... ");
  Serial.println(http.getIP());
  delay(1000);
}
int id = 0;


double lat = 0, lon = 0;

void loop() {
  while (ss.available() > 0){
    //Serial.write(ss.read());
    gps.encode(ss.read());
  }
  if (gps.location.isValid())
  {
    lat = gps.location.lat();
    lon = gps.location.lng();
  }
  //Serial.println(String(lat) + " " + String(lon));
  Serial.print("Get... ");
  String response =http.get("http://193.168.227.142:8000/location?device_id=" + String(device_id) + "&lat=" + String(int(lat)) + "." + String(int(lat * 1000000) % 1000000) +
   "&lon=" + String(int(lon)) + "." + String(int(lon * 1000000) % 1000000), true) ;
  Serial.println(response);
  String json_str = response.substring(response.indexOf("DATA:") + 5).c_str();
  char json[1024];
  json_str.toCharArray(json, json_str.length());
  StaticJsonDocument<1024> doc;
  deserializeJson(doc, json);
  Serial.println(String(doc["msg_id"].as<String>()) + " " + String(doc["text"].as<String>()));

  if (String(doc["msg_id"].as<String>()) != "-1"){
    if (deviceConnected) {
      static char msg_id_c[10] = {};
      String(doc["msg_id"].as<String>()).toCharArray(msg_id_c, String(doc["msg_id"].as<String>()).length());
      messageIdUUID.setValue(msg_id_c);
      messageIdUUID.notify();

      static char text_c[200] = {};
      String(doc["text"].as<String>()).toCharArray(text_c, String(doc["text"].as<String>()).length());
      textUUID.setValue(text_c);
      textUUID.notify();
    }
  }

  delay(1000);

}

