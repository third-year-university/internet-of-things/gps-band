#include <M5StickCPlus.h>
#include "pitches.h"
#include <BLEDevice.h>
#define BUZZZER_PIN  2

#define bleServerName "gps_device"
static BLEUUID bmeServiceUUID("c93bcec0-b099-479d-b455-0a00dfa81b30");

static boolean doConnect = false;
static boolean connected = false;

static BLEUUID textUUID("28612975-db8e-4870-aa13-18a1c293a062");
static BLEUUID messageIdUUID("3c0faebf-9417-4ef5-a97e-bb6634780d3d");

static BLERemoteCharacteristic* text;
static BLERemoteCharacteristic* messageId;

static BLEAddress *pServerAddress;

char* text_char;
char* messageId_char;

const uint8_t notificationOn[] = {0x1, 0x0};
const uint8_t notificationOff[] = {0x0, 0x0};

boolean newText = false;
boolean newMessageId = false;

bool connectToServer(BLEAddress pAddress) {
   BLEClient* pClient = BLEDevice::createClient();
 
  // Connect to the remove BLE Server.
  pClient->connect(pAddress);
  Serial.println(" - Connected to server");
 
  // Obtain a reference to the service we are after in the remote BLE server.
  BLERemoteService* pRemoteService = pClient->getService(bmeServiceUUID);
  if (pRemoteService == nullptr) {
    Serial.print("Failed to find our service UUID: ");
    Serial.println(bmeServiceUUID.toString().c_str());
    return (false);
  }
 
  // Obtain a reference to the characteristics in the service of the remote BLE server.
  text = pRemoteService->getCharacteristic(textUUID);
  messageId = pRemoteService->getCharacteristic(messageIdUUID);

  if (text == nullptr || messageId == nullptr) {
    Serial.print("Failed to find our characteristic UUID");
    return false;
  }
  Serial.println(" - Found our characteristics");
 
  //Assign callback functions for the Characteristics
  text->registerForNotify(textNotifyCallback);
  messageId->registerForNotify(messageIdNotifyCallback);
  return true;
}

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    if (advertisedDevice.getName() == bleServerName) { //Check if the name of the advertiser matches
      advertisedDevice.getScan()->stop(); //Scan can be stopped, we found what we are looking for
      pServerAddress = new BLEAddress(advertisedDevice.getAddress()); //Address of advertiser is the one we need
      doConnect = true; //Set indicator, stating that we are ready to connect
      Serial.println("Device found. Connecting!");
    }
  }
};

static void textNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                        uint8_t* pData, size_t length, bool isNotify) {
  //store temperature value
  text_char = (char*)pData + '\0';
  Serial.println(String(text_char));
  newText = true;
}

static void messageIdNotifyCallback(BLERemoteCharacteristic* pBLERemoteCharacteristic, 
                                    uint8_t* pData, size_t length, bool isNotify) {
  //store humidity value
  messageId_char = (char*)pData;
  newMessageId = true;
}

int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

void setup(){
  // Initialize the M5StickCPlus object. Initialize the M5StickCPlus object
  Serial.begin(115200);
  M5.begin();
  
  // LCD display. LCd display
  M5.Lcd.textsize = 3;
  M5.Lcd.print("Hello Lol");
  BLEDevice::init("");
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->start(30);
  }

void loop() {
  if (doConnect == true) {
    if (connectToServer(*pServerAddress)) {
      Serial.println("We are now connected to the BLE Server.");
      //Activate the Notify property of each Characteristic
      text->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      messageId->getDescriptor(BLEUUID((uint16_t)0x2902))->writeValue((uint8_t*)notificationOn, 2, true);
      connected = true;
    } else {
      Serial.println("We have failed to connect to the server; Restart your device to scan for nearby BLE server again.");
    }
    doConnect = false;
  }

  if (newText || newMessageId){
    newText = false;
    newMessageId = false;
    M5.Lcd.fillScreen(0);
    M5.Lcd.cursor_x = 0;
    M5.Lcd.cursor_y = 0;
    M5.Lcd.flush();
    M5.Lcd.print(String(text_char));
    
    for (int thisNote = 0; thisNote < 8; thisNote++) {
      int noteDuration = 1000 / noteDurations[thisNote];
      tone(BUZZZER_PIN, melody[thisNote], noteDuration);

      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);
      noTone(BUZZZER_PIN);
    }
  }
  Serial.println("Working ");
  delay(1000);
}