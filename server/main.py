 
from fastapi import FastAPI
import telebot
import threading
import signal

app = FastAPI()

owner_id=99908516
bot = telebot.TeleBot("2122784176:AAFRrHxma8GkuC-GUviM3WcTiFtiuoyuUJ0", parse_mode=None) # You can set parse_mode by default. HTML or MARKDOWN
my_lat=0.0
my_lon=0.0

loc_msg = bot.send_location(owner_id, latitude=my_lat, longitude=my_lon, live_period=86400).message_id

print(loc_msg)

msg_id = 0

msg_queue = []

stop_bot = False


@bot.message_handler(commands=['ans'])
def send_welcome(message):
    global msg_id
    global msg_queue
    bot.reply_to(message, "Msg id: " + str(msg_id))
    args = message.text.split()
    device_id = int(args[1])
    text = ""
    for i in range(2, len(args)):
        text += args[i] + " "
    print(text)
    msg_queue.append({'msg_id': msg_id, 'chat_msg': text, 'device_id': device_id})
    msg_id += 1


@app.get("/location")
async def root(device_id, lat, lon):
    global my_lat
    global my_lon
    global msg_queue
    print(device_id, lat, lon, msg_queue)
    if my_lat != lat and my_lon != lon:
        my_lat = lat
        my_lon = lon
        try:
            bot.edit_message_live_location(chat_id=owner_id, message_id=loc_msg, latitude=my_lat, longitude=my_lon)
        finally:
            pass
    msg_id = -1
    chat_msg = ""
    for i in range(len(msg_queue)):
        if msg_queue[i]['device_id'] == int(device_id):
            msg_id = msg_queue[i]['msg_id']
            chat_msg = msg_queue[i]['chat_msg']
            msg_queue.pop(i)
    return {"message": "OK", 'msg_id': msg_id, 'text': chat_msg}


def thread_function():
    while not stop_bot:
        print(str(stop_bot))
        try:
            bot.polling()
        except:
            print("something happend")


x = threading.Thread(target=thread_function)
x.start()

def handler(signum, frame):
    print('starting finishing')
    global stop_bot
    stop_bot = True
    global x
    x.join()


@app.on_event("startup")
async def startup_event():
    import signal
    signal.signal(signal.SIGINT, handler)
